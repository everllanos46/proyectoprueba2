import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableNativeFeedback,
  Alert,
  Button,
} from "react-native";

export default function App() {
  const [name, setName] = useState("");
  return (
    <View style={styles.container}>
      <TouchableNativeFeedback onPress={() => Alert.alert("Alerta")}>
        <Text>Hola mundo</Text>
      </TouchableNativeFeedback>

      <TextInput
        style={{ borderBottomWidth: 1.0, marginTop: 10, marginVertical: 8 }}
        onChangeText={setName}
        value={name}
      />
      <Button
        title="Hola"
        onPress={() => Alert.alert(name)}
        style={{ marginVertical: 8, marginTop: 18 }}
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
